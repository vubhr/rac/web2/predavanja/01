<?php
// Prikazivanje svih grešaka
error_reporting(E_ALL);

// Uključivanje prikaza grešaka na stranici
ini_set('display_errors', 1);

//ako ne želite prikazivati greške onda
//ini_set('display_errors', 0);
//ili u php.ini display_errors=Off

// Greška
echo $nePostojiVarijabla;
?>
