<?php
$ime = 'Lucija';
$brojGodina = 28;
$visina = 173.45;
/* blok koda se može
    zakomentirati na ovaj način */
//echo 'Ime: ', $ime, 'broj godina: ', $brojGodina, ' visina: ', $visina;
//print $visina;
// je komentar za jednu liniju i na ovaj način lako deaktivirano neku liniju koda u PHP-u
# ovo je drugi znak za zakomentirati liniju u PHP-u ' ' " "
//echo 'Navodnici imaju problem isto k\'o u svim drugim programskim jezicima';
//echo "Navodnici imaju problem isto k'o u svim drugim programskim jezicima";
//echo '"Navodnici imaju problem isto k\'o u svim drugim programskim jezicima"';
$voće = "Banane";
//echo '$ime voli $voće';

echo "$ime voli $voće";
?>